package at.halboffen.scala.genetix.world

import scala.util.Random

/**
  * The singleton object "World" represents the world our individuals live in. It is responsible
  * for creating an initial population, controlling mutation and recombination of the
  * population. And finally deciding what individuals survive in the population.
  */
object World {

  /** A population is represented by a "Vector" */
  type Population = Vector[Individual]

  /** Number of individuals in the population. */
  var populationSize = 100

  /** Number of epochs (generations) of the simulation. */
  var iterations = 100

  /** Factory method to create an individual. Has to be supplied from outside. */
  var individualFactory: () => Individual = _

  /** Method to evaluate an individual. Has to be supplied from outside. */
  var evaluateIndividual: (Individual) => Double = _

  /** The most successful individual up to date (current and past populations). */
  protected var bestGlobal: Individual = _

  /** The most successful individual in the current population. */
  var best: Individual = _

  /** The weakest individual in the current population. */
  var weakest: Individual = _

  var beforeInit: () => Unit = () => {}
  var afterInit: Population => Unit = (_: Population) => {}
  var beforeStep: (Population, Int) => Unit = (_: Population, _: Int) => {}
  var afterStep: (Population, Int) => Unit = (_: Population, _: Int) => {}
  var whenFinished: Population => Unit = (_: Population) => {}

  /** Run the simulation. */
  def run(): Unit = {
    beforeInit()
    var population = firstGeneration
    afterInit(population)

    for (i <- 1 to iterations) {
      beforeStep(population, i)
      population = nextGeneration(population)
      afterStep(population, i)
    }

    whenFinished(population)
  }

  /**
    * Create first generation, initialize it and evaluate fitness of individuals in the new population.
    *
    * @return New population
    */
  def firstGeneration: Population = evaluatePopulation(initialize)

  /**
    * Create next generation by recombination, mutation and selection.
    *
    * @param population Old population
    * @return New population
    */
  def nextGeneration(population: Population): Population =
    selectPopulation(population ++ recombinePopulation(population) ++ mutatePopulation(population))

  /**
    * Create an initial population of individuals.
    *
    * @return Initial population.
    */
  private def initialize: Population = Iterator.continually{individualFactory()}.take(populationSize).toVector


  /**
    * Mutate the population. The genetic operation of mutation is applied
    * to all individuals.
    *
    * @param population Population to be mutated.
    * @return New population of mutated individuals.
    */
  private def mutatePopulation (population: Population): Population = {
    var tmpPopulation = Vector.empty[Individual]

    for (individual <- population) {
      individual.mutate match {
        case Some(x) => tmpPopulation = tmpPopulation :+ x
        case None    => null
      }
    }
    tmpPopulation
  }

  /**
    * Recombine the population. The genetic operation of recombination is applied
    * to all individuals. Each individual gets a randomly chosen mate. New individuals
    * are added to the population, old individuals remain in the population.
    *
    * @param population Population to be recombined.
    * @return New population of recombined individuals.
    */
  private def recombinePopulation (population: Population): Population = {
    var tmpPopulation = Vector.empty[Individual]

    for (individual <- population) {
      // find a random mate
      val mate = population(Random.nextInt(population.length))
      individual.recombine(mate) match {
        case Some(x) => tmpPopulation = tmpPopulation :+ x
        case None    => null
      }
    }
    tmpPopulation
  }

  /**
    * Choose those individuals that survive for the next generation. The probability to survive
    * is directly proportional to its fitness.
    *
    * @param population Population before selection.
    * @return Population after selection.
    */
  private def selectPopulation (population: Population): Population = {
    var tmpPopulation = Vector.empty[Individual]
    evaluatePopulation(population)

    val sumFitness = population.foldLeft(0.0)((x: Double, y: Individual) => x + y.normalizedFitness)
    val start = Random.nextFloat() * sumFitness / population.length
    val step = sumFitness / populationSize
    var j = 0
    var sum = population(0).normalizedFitness
    for (i <- 0 until populationSize) {
      val limit = start + i * step
      while (sum < limit) {
        j += 1
        sum += population(j).normalizedFitness
      }
      tmpPopulation = tmpPopulation :+ population(j)
    }
    tmpPopulation
  }

  /**
    * Calculate the fitness of all individuals in the population.
    *
    * @param population Population to calculate fitness for.
    */
  private def evaluatePopulation (population: Population): Population = {

    population.foreach((individual: Individual) => individual.fitness = evaluateIndividual(individual))
    best = population.foldLeft(population(0))((p1, p2) => if (p1.fitness > p2.fitness) p1; else p2)
    weakest = population.foldLeft(population(0))((p1, p2) => if (p1.fitness < p2.fitness) p1; else p2)
    if (bestGlobal == null || bestGlobal.fitness < best.fitness)
      bestGlobal = best
    // Normalize
    population.foreach((x: Individual) => x.normalizedFitness = (x.fitness - weakest.fitness) / (best.fitness - weakest.fitness))
    population
  }

  /**
    * Print the population to stdout.
    *
    * @param population The population to be printed.
    */
  def print (population: Population): Unit = {
    var mean = 0.0
    for (i <- population) {
      mean += i.fitness
      println(i)
    }
    println("Mean Fitness: " + (mean / population.length))
    println("Best:        " + (if (best == null) "" else best.toString))
    println("Weakest:     " + (if (best == null) "" else weakest.toString))
    println("Best Global: " + (if (bestGlobal == null) "" else bestGlobal.toString))
  }

}
