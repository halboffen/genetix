package at.halboffen.scala.genetix

/**
  * Provides classes to represent the world, in which the genetic evolution finds place. The class
  * [[at.halboffen.scala.genetix.world.World]] executes genetic operations of mutation, recombination
  * and selection upon the population of [[at.halboffen.scala.genetix.world.Individual]]s.
  *
  * By selecting the fittest individuals for the next generation, the population should evolve to become more
  * and more successful on the give problem.
  */
package object world { }
