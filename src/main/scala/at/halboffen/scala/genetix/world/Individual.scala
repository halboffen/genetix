package at.halboffen.scala.genetix.world

/**
  * Individual represents a member of the genetic population. The representation
  * of the solution must be defined by the implementing class.
  */
trait Individual {

  /**
    * Mutate an individual.
    *
    * @return The mutated individual as an optional value.
    */
  def mutate: Option[Individual]

  /**
    * Recombine (=mate) two individuals.
    *
    * @param individual Individual to mate with.
    * @return New individual created by mating.
    */
  def recombine(individual: Individual): Option[Individual]

  /**
    * A string representation of an individual.
    *
    * @return String representation of individual.
    */
  override def toString: String

  /**
    * Fitness of the individual. The higher the fitness, the more likely
    * the individual will survive.
    */
  var fitness: Double = _

  /**
    * Property to cache a normalized fitness, e.g. scaled to an interval of [0, 1].
    */
  var normalizedFitness: Double = _
}
