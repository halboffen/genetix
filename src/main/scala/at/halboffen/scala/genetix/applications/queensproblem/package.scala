package at.halboffen.scala.genetix.applications

import at.halboffen.scala.genetix.chromosome.SeqChromosome
import at.halboffen.scala.genetix.gene.GeneInt
import at.halboffen.scala.genetix.world.Individual

import scala.util.Random

/**
  * Provides definition of genetic operators to solve the n queens problem.
  *
  * The eight queens puzzle is the problem of placing eight chess queens on an 8×8 chessboard
  * so that no two queens threaten each other. Thus, a solution requires that no two queens
  * share the same row, column, or diagonal. You can generalize the eight queens problem to
  * an n queens problem where n is a positive natural number.
  *
  * Every individual of the genetic population represents a (valid or invalid) solution to the
  * problem. A solution is encoded into a sequence of n integers. The n-th integer represents
  * the column number of the queen in row n.
  *
  * Two functions have to be provided:
  *
  *  - A function to generate an individual of the genetic population
  *  - A function to evaluate the fitness of an individual. The fitness reflects the quality of the solution.
  */
package object queensproblem {

  /** Number of queens */
  val queens = 8

  /** Mutation probability */
  val mutationProbability = 0.01

  /** Population size */
  val populationSize = 100

  /** Maximum number of iterations */
  val maxIterations = 100

  /** Evaluation function to compute the fitness of a (possible invalid) solution. The less conflicts
    * exist, the higher fitness is calculated. If there are no conflicts, fitness is zero. If there are
    * n conflicts, fitness is -n.
    *
    * @param individual The individual (=solution) to evaluate fitness for.
    * @return The fitness of the individual (=solution).
    */
  def evaluateIndividual(individual: Individual): Double = {
    // We can upcast Individual to Chromosome and
    // Gene to GeneInt, because it has been created
    // with these types in the same class.
    val chromosome =
      individual match {
        case x: SeqChromosome => x
        case _ => throw new ClassCastException("Individual is not a Chromosome")
      }
    var conflicts = 0
    for (rowA <- 0 to chromosome.value.length - 2)
      for (rowB <- rowA+1 until chromosome.value.length) {
        val (colA, colB) =
          (chromosome.value(rowA).value, chromosome.value(rowB).value) match {
            case (x: Int, y: Int) => (x, y)
            case _ => throw new ClassCastException("Gene is not a GeneInt")
          }
        if (colA == colB || Math.abs(colA - colB) == rowB - rowA)
          conflicts += 1
      }
    -conflicts.toDouble
  }

  /**
    * Creates random chromosomes (=representation of the chess board) for an individual.
    * This function is used for initializing a random (probably invalid) solution.
    *
    * @return Randomly generated chromosome.
    */
  def createChromosome(): SeqChromosome = {
    val genes = Vector.fill(queens)(GeneInt(Random.nextInt(queens), 1, queens, mutationProbability))
    new SeqChromosome(genes)
  }

}
