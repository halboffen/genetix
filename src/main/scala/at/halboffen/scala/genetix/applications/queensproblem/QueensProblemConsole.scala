package at.halboffen.scala.genetix.applications.queensproblem

import at.halboffen.scala.genetix.world.World
import at.halboffen.scala.genetix.world.World.Population

/**
  * Console version of the queens problem. All output goes to stdout.
  */
object QueensProblemConsole {

  /**
    * Run the simulation.
    *
    * @param args Commandline arguments are not used.
    */
  def main(args: Array[String]): Unit = {

    World.individualFactory = createChromosome
    World.evaluateIndividual = evaluateIndividual
    World.populationSize = 100
    World.iterations = 100

    // Define callback hooks for the running simulation.
    World.afterStep = (_: Population, iteration: Int) => println("New Generation " + iteration)
    World.whenFinished = (population: Population) => World.print(population)
    World.run()

  }
}
