package at.halboffen.scala.genetix.applications.queensproblem

import java.awt.Font

import at.halboffen.scala.genetix.world.World.Population
import at.halboffen.scala.genetix.world.{Individual, World}
import at.halboffen.scala.genetix.chromosome.SeqChromosome

import scala.swing._
import scala.swing.event.ButtonClicked
import scala.util.Random

/**
  * Graphical user interface for the queens problem based on scala swing.
  */
object QueensProblemGUI extends SimpleSwingApplication {

  /**
    * Panel that displays a chess board.
    */
  protected class ChessBoard extends BorderPanel {
    val grid = new GridPanel(queens, queens)
    add(grid, BorderPanel.Position.Center)
    val label = new Label("")
    add(label, BorderPanel.Position.South)
  }

  /** Chess board for best solution found until now */
  val boardBest = new ChessBoard() {
    add(new Label("Best Solution"), BorderPanel.Position.North)
  }

  /** Chess board for worst solution found until now */
  val boardWeakest = new ChessBoard() {
    add(new Label("Worst Solution"), BorderPanel.Position.North)
  }

  /** Chess board with a randomly chosen solution from the population */
  val boardRandom1 = new ChessBoard() {
    add(new Label("Random Solution"), BorderPanel.Position.North)
  }

  /** Another chess board with a randomly chosen solution from the population */
  val boardRandom2 = new ChessBoard() {
    add(new Label("Random Solution"), BorderPanel.Position.North)
  }

  /** Label displaying a queen. It is a symbol of the unicode character set */
  protected class QueenSquare extends Label("\u2655") {
    font = new Font("Sans-Serif", Font.PLAIN, 40)
    border = Swing.LineBorder(java.awt.Color.BLACK, 1)
  }

  /** Empty label for an empty square. */
  protected class EmptySquare extends Label("") {
    border = Swing.LineBorder(java.awt.Color.BLACK, 1)
  }

  /** Button to continue simulation. It is displayed every [[stepSize]] steps. */
  val nextButton = new Button()

  /** Population of individuals */
  var population: Population = _

  /** Number of the last finished iteration of the simulation */
  var lastIteration = 0

  /** Flag if simulation has finished, e.g. [[maxIterations]] is reached. */
  var finished = false

  /** Number of iterations to calculate when Button [[nextButton]] is pressed. */
  val stepSize = 10

  /** Entry point of the swing application */
  def top = new MainFrame() {
    World.individualFactory = createChromosome
    World.evaluateIndividual = evaluateIndividual
    World.populationSize = populationSize
    World.iterations = maxIterations

    population = World.firstGeneration

    title = "Queens Problem"
    preferredSize = new Dimension(800, 1000)

    contents = new BoxPanel(Orientation.Vertical) {
      contents += new GridPanel(2, 2) {
        vGap = 50
        hGap = 50
        contents += boardBest
        contents += boardWeakest
        contents += boardRandom1
        contents += boardRandom2
      }
      contents += nextButton
    }

    nextButton.text = s"Iterations ${lastIteration+1} to ${lastIteration+stepSize}"

    showStep()

    listenTo(nextButton)

    reactions += {
      case ButtonClicked(_) => nextStep()
    }
  }

  /**
    * Fills the chessboard with queens according to given individual.
    *
    * @param board Chessboard to fill
    * @param individual Individual that represents the solution to be displayed.
    */
  def fillBoard (board: ChessBoard, individual: Individual): Unit = {
    val chromosome = individual match {
      case x: SeqChromosome => x
      case _ => throw new ClassCastException("Individual is not a chromosome")
    }

    board.grid.contents.clear()
    for (row <- 0 until queens)
      for (col <- 0 until queens)
        if (chromosome.value(col).value == row)
          board.grid.contents += new QueenSquare
        else
          board.grid.contents += new EmptySquare

    board.label.text = f"Conflicts: ${-individual.fitness}%2.0f"
  }

  /**
    * Displays four individuals of the current simulation state:
    *  - the best solution known yet (of the current or past population),
    *  - the worst solution of the population,
    *  - two randomly chosen solutions.
    */
  def showStep (): Unit = {
    fillBoard(boardBest, World.best)
    fillBoard(boardWeakest, World.weakest)
    fillBoard(boardRandom1, population(Random.nextInt(population.length)))
    fillBoard(boardRandom2, population(Random.nextInt(population.length)))
  }

  /**
    * Takes action when user presses [[nextButton]].
    *
    * Calculates the next [[stepSize]] Iterations and displays the result
    * by calling [[showStep]].
    *
    * Finishes the application, if flag [[finished]] is true.
    */
  def nextStep (): Unit = {
    // User pressed "Finish"?
    if (finished)
      quit()
    else {
      val step = Math.min(stepSize, World.iterations - lastIteration)
      for (_ <- 1 to step)
        population = World.nextGeneration(population)
      lastIteration += step
      showStep()
      // Did we reach the maximum of iterations?
      if (lastIteration >= World.iterations) {
        nextButton.text = "Finish"
        finished = true
      }
      else nextButton.text = s"Iterations ${lastIteration+1} to ${lastIteration+Math.min(stepSize, World.iterations-lastIteration)}"
    }
  }
}
