package at.halboffen.scala.genetix

/**
  * Provides classed to represent genes. A gene is the smallest information of a chromosome.
  * Each chromosome consists of a number of genes. A gene can have any type.
  *
  * The trait [[at.halboffen.scala.genetix.gene.Gene]] must be extended by all classes representing a Gene.
  */
package object gene { }
