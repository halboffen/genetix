package at.halboffen.scala.genetix.gene

import scala.util.Random

/**
  * A gene is the smallest unit of information in a chromosome. It can have any type
  * (e.g. Int, Char, String,...).
  */
trait Gene {

  /** Type of the gene. */
  type T

  /** The actual value the gene takes. */
  val value: T

  /** Probability that a gene mutates each generation. */
  protected val mutationProb: Double

  /** The actual operation of mutating a gene. */
  protected val mutator: () => Gene

  /**
    * General function to mutate a gene with certain probability.
    *
    * @return The mutated gene as an optional value.
    */
  def mutate(): Option[Gene] = {
    if (Random.nextDouble() < mutationProb)
      Some(mutator())
    else
      None
  }

}
