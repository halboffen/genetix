package at.halboffen.scala.genetix.gene

import scala.util.Random

/**
  * A gene represented by an integer value.
  */
case class GeneInt (value: Int, min: Int, max: Int, mutationProb: Double) extends Gene {

  /** The type of the gene is an integer */
  type T = Int

  /** Mutation is done by creating a copy of self with a new random value */
  protected val mutator: () => GeneInt = () => this.copy(value = min + Random.nextInt(max))

}
