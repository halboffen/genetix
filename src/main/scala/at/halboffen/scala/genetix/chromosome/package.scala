package at.halboffen.scala.genetix

/**
  * Provides classes to represent a chromosome, e.g. data structures to hold genes. These
  * chromosomes represent a solution to a give problem. Chromosomes are changed by genetic
  * operations like mutation and recombination to find better solutions.
  *
  * Every chromosome must extend [[at.halboffen.scala.genetix.world.Individual]].
  */
package object chromosome { }
