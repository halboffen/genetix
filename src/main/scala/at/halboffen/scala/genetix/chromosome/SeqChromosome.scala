package at.halboffen.scala.genetix.chromosome

import scala.util.Random

import at.halboffen.scala.genetix.gene.Gene
import at.halboffen.scala.genetix.world.Individual

/**
  * A sequential chromosome is an individual that is defined by a sequence of genes.
  *
  * The sequence of genes represents the solution to the problem. This follows the encoding
  * of classic genetic algorithms.
  */
class SeqChromosome(val value: Vector[Gene]) extends Individual {

  /**
    * Genetic operator for mutation.
    *
    * Iterates all genes of the chromosome and calls the mutation
    * operator on each gene.
    *
    * @return The mutated individual as an optional value. Nothing
    *         is returned, if no genes where mutated.
    */
  def mutate: Option[SeqChromosome] = {
    var newValue: Option[Vector[Gene]] = None
    for (i <- value.indices) {
      val nv = value(i).mutate()
      (nv, newValue) match {
        case (Some(x: Gene), None) => newValue = Some(value.updated(i, x))
        case (Some(x: Gene), Some(y)) => newValue = Some(y.updated(i, x))
        case _ => None
      }
    }
    newValue match {
      case Some(x) => Option(new SeqChromosome(x))
      case _ => None
    }
  }

  /**
    * Genetic operator for recombination.
    *
    * Produces a new individual that is a genetic recombination
    * of two individuals (this and mate).
    *
    * @param mate Individual to mate with.
    * @return New individual created by mating.
    */
  def recombine(mate: Individual): Option[Individual] = {
    // A chromosome can only be mated with another chromosome
    // and no other implementation of individual (e.g. an
    // individual that has a tree of genes as proposed by Koza).
    val castedMate =
      mate match {
        case mate: SeqChromosome => mate
        case _ => throw new ClassCastException("individual is no chromosome")
      }
    val length = Math.min(value.length, castedMate.value.length)
    val cutIndex = 1 + Random.nextInt(length-2)
    Option(new SeqChromosome(value.slice(0, cutIndex) ++ castedMate.value.slice(cutIndex, castedMate.value.length)))
  }

  /** String representation of a chromosome */
  override def toString: String = value.map(_.value).mkString(",") + ": " + this.fitness

}

