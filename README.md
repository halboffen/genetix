# GENETIX #

## What is it? ##

Genetix is an extensible library for evolutionary computational models. 
Currently it consists of a classic genetic algorithm based on integers.
It can easily be extended to different data types and different evolutionary
strategies like genetic programming on context free languages as proposed
by John R. Koza.

Classic genetic algorithms are inspired by biological processes and are
commonly used to solve optimization and search problems. In a genetic
algorithm a population of candidate solutions, called individuals, is
optimized by means of genetic operations known from evolution:

 - Mutation: the genetic information of a solution is changed randomly.
 - Recombination: individuals mate and produce new individuals.
 - Selection: indivdiuals with higher fitness have a higher probability
   of survival.
   
The genetic information representing the solution is encoded as a sequence of
Tokens, e.g. integer values.

Genetix includes a sample application, the n queens problem, which makes
use of the genetix library. There are two frontends to run this problem:
a console based and a graphical frontend based on swing.

## How to build and run it? ##

### Prerequisites ###

You need the following software components installed on your system:

 - Java JDK 8
 - Scala 2.12.1+
 - sbt 0.13.15+
 
It might run on earlier versions, but this is not tested.

### Building ###

```bash
> git clone git@bitbucket.org:halboffen/genetix.git
> cd genetix
> sbt package
```

### Running ###

Locate the Jar-File. It might be named like genetix_2.12-1.0.jar.

Run the console version:

```bash
> scala -cp [path/to/jar]genetix_2.12-1.0.jar at.halboffen.scala.genetix.applications.queensproblem.QueensProblemConsole
```

Run the graphical version:

```bash
> scala -cp [path/to/jar]genetix_2.12-1.0.jar at.halboffen.scala.genetix.applications.queensproblem.QueensProblemGUI
```

## Project layout ##

 - src/test/scala: test cases do not exist yet
 - src/main/scala/at/halboffen/scala/genetix/[gene,chromosome,world]: genetic library
 - src/main/scala/at/halboffen/scala/genetix/applications: use cases of the library (e.g. n queens problem)
 - docs: scaladoc

## Authors ##

 - Peter Bruhn ([peter.bruhn@halboffen.at](mailto:peter.bruhn@halboffen.at"), [halboffen.at](http://halboffen.at))